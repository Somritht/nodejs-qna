class ErrorResponse {
    constructor(res,status,error) {
        return res.status(status).json({message: error});
    }
}

module.exports = ErrorResponse