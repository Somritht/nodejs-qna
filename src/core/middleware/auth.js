const jwt = require("jsonwebtoken");
const env = require("../../../env.js");

const verify = function (req, res, next) {
    const token = req.header("token");
    if (!token) return res.status(401).json({ message: "Auth Error" });
    try {
        const decoded = jwt.verify(token, env.jwtSecret);
        req.user = decoded.data;
        console.log(req.user);
        next();
    } catch (e) {
        console.error(e);
        return res.status(500).send({ message: "Invalid Token" });
    }
};

const getUser = function (req) {
    const token = req.header("token");
    if (!token) return {};
    try {
        const decoded = jwt.verify(token, env.jwtSecret);
        req.user = decoded.data;
        return { ...req.user };
    } catch (e) {
        return {};
    }
};

module.exports = { verify, getUser };