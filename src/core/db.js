const db = new sqlite3.Database("public/my.db");

export class Db {
    dbName;
    constructor(databaseName) {
        this.dbName = databaseName;
    }

    async all() {
        db.all(`SELECT * FROM ${this.dbName}`, (error, rows) => {
            if (error) new ErrorResponse(res, 400, 'server error');
            forEach(rows, (row) => {
                console.log(row.username + " " + row.email);
            });
            return res.json(rows);

            return res.status(200).send({
                message: "success"
            });
        });
    }
}

module.exports = Db;