var router = require("express").Router();
const sqlite3 = require("sqlite3");
const db = new sqlite3.Database("src/data/my.db");
const Joi = require('joi');
var ErrorResponse = require('../core/error.js');
const {verify, getUser } = require("../core/middleware/auth");

const createQuestionSchema = Joi.object({
    title: Joi.string().min(3).max(30).required(),
    body: Joi.string().min(3).max(200).required()
})

const createAnswerSchema = Joi.object({
    body: Joi.string().min(3).max(200).required()
})

router.get("/list", async (req, res) => {
    let limit = 15;
    let offset = 0;
    const { page } = req.query;
    const number = Joi.number();   
    if(!number.validate(page))
    return new ErrorResponse(res, 400, 'error');
    if(page)
    {
        offset = (limit * (page-1));
    }

    try {
        db.all(`SELECT * FROM questions LIMIT ${limit} OFFSET ${offset}`, (error, rows) => {
            if (error) new ErrorResponse(res, 400, 'server error');
            return res.status(200).send({
                message: "success",
                data: rows,
                pagination: {
                    previous_page: (page > 1) ? page-1 : null,
                    next_page: null,
                    current_page: page,
                    page_count: null
                },
                params: req.query
            });
        });
    } catch (e) {
        return res.status(200).send({
            message: "fail"
        });
    }
})

router.get("/list/:id", async (req, res) => {
    const number = Joi.number();   
    const { id } = req.params;
    if(!number.validate(id))
    return new ErrorResponse(res, 400, 'error');

    try {
        db.all(`SELECT * FROM questions WHERE id = ${id}`, (error, rows) => {
            if (error) return new ErrorResponse(res, 400, 'server error');
            if (rows.length <= 0) return new ErrorResponse(res, 400, 'record not found');
            const questions = rows[0];
            db.all(`SELECT * FROM answers WHERE question_id = ${id}`,(error,rows) => {
                return res.status(200).send({
                    message: "success",
                    data: {...questions, answer : { rows } }
                });
            })
            
        });
    } catch (e) {
        return res.status(200).send({
            message: "fail"
        });
    }
})

router.post("/create", verify, async (req, res) => {

    const { title, body } = req.body;
    const { error } = createQuestionSchema.validate({ title, body });
    if (error) return new ErrorResponse(res, 400, error.details);
    db.run(`
    INSERT INTO questions 
    (title,body,created_by)
     VALUES  
     ('${title}','${body}','${getUser(req).name}')
     `, [], (error) => {
        if (error) return new ErrorResponse(res, 400, 'server error');
        return new ErrorResponse(res, 200, {
            title: title,
            body: body,
            created_by: getUser(req).name
        });
    });
})

router.post("/answer/:id", verify, async (req, res) => {

    const number = Joi.number();   
    const { id } = req.params;
    if(!number.validate(id))
    return new ErrorResponse(res, 400, 'error');

    const { body } = req.body;
    const { error } = createAnswerSchema.validate({ body });
    if (error) return new ErrorResponse(res, 400, error.details);

    db.run(`
    INSERT INTO answers 
    (question_id, body, created_by)
    VALUES  
    (   '${id}',
        '${body}',
        '${getUser(req).name}'
    )
     `, [], (error) => {
        if (error) return new ErrorResponse(res, 400, 'server error');
        return new ErrorResponse(res, 200, {
            body: body,
            created_by: getUser(req).name
        });
    });
})

module.exports = router;