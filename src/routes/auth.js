var router = require("express").Router();
var bcrypt = require("bcryptjs");
const sqlite3 = require("sqlite3");
const db = new sqlite3.Database("src/data/my.db");
const Joi = require('joi');
var ErrorResponse = require('../core/error.js');
var jwt = require("jsonwebtoken");
const env = require("../../env.js");
const {verify } = require("../core/middleware/auth");

const schema = Joi.object({
    username: Joi.string().alphanum().min(3).max(30).required(),
    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com'] } })
})

router.get("/user", verify, async (req, res) => {
    try {
        db.all("SELECT * FROM users", (error, rows) => {
            if (error) new ErrorResponse(res, 400, 'server error');
            return res.status(200).send({
                message: "success",
                data: rows
            });
        });
    } catch (e) {
        console.log(e);
        return res.status(200).send({
            message: "fail"
        });
    }
})

router.post("/register", (req, res, next) => {
    const { username, email, password } = req.body;
    const { error } = schema.validate({ username, email, password });
    if (error) new ErrorResponse(res, 400, error.details);


    db.all(`SELECT * FROM users WHERE email = '${email}'`, [], (error, rows) => {
        if (error) return new ErrorResponse(res, 400, 'server error');
        if (rows.length > 0) return new ErrorResponse(res, 400, 'email exist');
        var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(password, salt);
        db.run(`
            INSERT INTO users 
            (name,email,password)
             VALUES  
             ('${username}','${email}','${hash}')
             `, [], (error) => {
            if (error) return new ErrorResponse(res, 400, 'server error');
            return new ErrorResponse(res, 200, 'success');
        });
    });
});

router.post("/login", (req, res, next) => {
    const { email, password } = req.body;
    try {
        db.all(`SELECT * FROM users where email ='${email}' LIMIT 1`, [],
            (error, rows) => {
                if (error) return new ErrorResponse(res, 400, { error: error });
                if (rows.length <= 0) return new ErrorResponse(res, 400, { error: 'user is not exist' });
                if (bcrypt.compareSync(password, rows[0].password)) {
                    console.log({ email: rows[0].email, name: rows[0].name });
                    let token = jwt.sign(
                        {
                            exp: Math.floor(Date.now() / 1000) + 60 * 60,
                            data: { email: rows[0].email, name: rows[0].name },
                        },
                        env.jwtSecret
                    );
                    return res.status(200).send({ ...req["body"], token });
                }
                return new ErrorResponse(res, 400, { error: 'error' });
            }
        );
    } catch (e) {
        return res.status(400).send({ message: "not have permission to access" });
    }
});

module.exports = router;