const sqlite3 = require("sqlite3");
const db = new sqlite3.Database("src/data/my.db");

db.run("CREATE TABLE questions([id] INTEGER PRIMARY KEY AUTOINCREMENT, [title] NVARCHAR(120), [body] NVARCHAR(120), [created_by] NVARCHAR(120), [created_at] DATETIME DEFAULT CURRENT_TIMESTAMP)");
db.run("CREATE TABLE answers([id] INTEGER PRIMARY KEY AUTOINCREMENT, [question_id] integer, [body] NVARCHAR(120), [created_by] NVARCHAR(120), [created_at] DATETIME DEFAULT CURRENT_TIMESTAMP)");
db.run("CREATE TABLE users([name] NVARCHAR(120),[email] NVARCHAR(120),[password] NVARCHAR(190), [created_at] DATETIME DEFAULT CURRENT_TIMESTAMP)");
