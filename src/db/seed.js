const sqlite3 = require("sqlite3");
const db = new sqlite3.Database("public/my.db");
var bcrypt = require("bcryptjs");

var salt = bcrypt.genSaltSync(10);
var hash = bcrypt.hashSync('12345', salt);

db.run(`
    INSERT INTO users 
    (
        name,
        email,
        password
    )
    VALUES  
    (
        'admin',
        'admin@gmail.com',
        '${hash}'
    )
    `, [], (error) => {
    
});

db.run(`
    INSERT INTO questions 
    (
        title,
        body,
        created_by
    )
    VALUES  
    (
        'best language for programming?',
        'I want to look for best lang for programming anyone pleas help',
        'Jonh Dev'
    )
    `, [], (error) => {
    
});

db.run(`
    INSERT INTO answers 
    (
        question_id,
        body,
        created_by
    )
    VALUES  
    (
        1,
        'all i know is html and css i guess',
        'Jonh Dev'
    )
    `, [], (error) => {
    
});