const sqlite3 = require("sqlite3");
const db = new sqlite3.Database("public/my.db");

db.run("DROP TABLE users");
db.run("DROP TABLE questions");
db.run("DROP TABLE answers");