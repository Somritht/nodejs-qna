var express = require("express");
var app = express();
var bodyParser = require("body-parser");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

var authRoute = require('./routes/auth.js');
var qaRoute = require('./routes/qa.js');

app.use('/api/auth', authRoute);
app.use('/api/qna', qaRoute);

app.get('*', function (req, res) {
    res.status(404).json({ message: 'not found ' });
})

app.listen(3000, () => {
    console.log("Server running on port 3000");
});
